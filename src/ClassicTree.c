#include <stdio.h>
#include "tree.h"

#define LEFT '4'
#define RIGHT '6'
#define PREVIOUS '8'
#define EXIT '0'

// Print choice
void print_choice(char choice) {
  switch (choice) {
    case LEFT:
      printf("left ");
      break;

    case RIGHT:
      printf("right ");
      break;

    case PREVIOUS:
      printf("previous ");
      break;
  }
}

/// Print node string
void print_node(char choice, const char* str) {
  print_choice(choice);
  printf("leads to node %s\n", str);
}

/// Print some info when hitting a dead end
void print_dead_end(char choice) {
  print_choice(choice);
  printf("leads to a dead end\n");
}

int main() {
  printf("       +---+\n"
         "       |   |\n"
         "       | a |\n"
         "       |   |\n"
         "  +----+---+----+\n"
         "+-v-+         +-v-+\n"
         "|   |         |   |\n"
         "| b |         | c |\n"
         "|   |         |   |\n"
         "+---+      +--+---+--+\n"
         "           |         |\n"
         "         +-v-+     +-v-+\n"
         "         |   |     |   |\n"
         "         | d |     | e |\n"
         "         |   |     |   |\n"
         "         +---+--+  +---+\n"
         "                |\n"
         "              +-v-+\n"
         "              |   |\n"
         "              | f |\n"
         "              |   |\n"
         "              +---+\n");

  Node const* current = &a;
  printf("you start at node %s\n", current->str);
  printf("\n");

  for (;;) {
    char c = getchar();

    switch (c) {
      case LEFT:
        if (current->left) {
          current = current->left;
          print_node(LEFT, current->str);
        } else {
          print_dead_end(LEFT);
        }
        break;

      case RIGHT:
        if (current->right) {
          current = current->right;
          print_node(RIGHT, current->str);
        } else {
          print_dead_end(RIGHT);
        }
        break;

      case PREVIOUS:
        if (current->previous) {
          current = current->previous;
          print_node(PREVIOUS, current->str);
        } else {
          print_dead_end(PREVIOUS);
        }
        break;

      case EXIT:
        printf("bye\n");
        return 0;
        break;

      default:
        printf("wrong input\n");
        break;
    }

    // get rid of enter
    c = getchar();

    printf("\n");
  }

  return 0;
}
