#include <stddef.h>
#include "tree.h"

const Node a, b, c, d, e, f;

const Node a = {"a", &b, &c, NULL};
const Node b = {"b", NULL, NULL, &a};
const Node c = {"c", &d, &e, &a};
const Node d = {"d", NULL, &f, &c};
const Node e = {"e", NULL, NULL, &c};
const Node f = {"f", NULL, NULL, &d};
