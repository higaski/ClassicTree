#pragma once

typedef struct Node {
  const char* str;
  const struct Node* left;
  const struct Node* right;
  const struct Node* previous;
} Node;

extern const Node a, b, c, d, e, f;
